const Product = require('../models/product');
const TokenController = require('./tokenController');
const jwt = require('jsonwebtoken');

class ProductController{

    constructor(){
        //Crear atributo de la clase de tipo TokenController
        this.tokenC = new TokenController();
    }

    create = (req, res)=>{
        //Obtener datos del cuerpo de la petición
        let {name, price, url_img} = req.body;
        //Obtener token
        let token = this.tokenC.getToken(req);
        //Decodificar token
        let decode = jwt.decode(token, process.env.NODE_PRIVATE_KEY);    
        //Obtener el id del usuario a partir del token
        let user_id = decode.id;    
        //Crear producto en la BD
        Product.create({name, price, url_img, user_id}, (error, doc)=>{
            if(error){
                res.status(500).json({error});
            }else{
                res.status(201).json(doc);
            }
        });
    }

    getByUser = (req, res)=>{
        //Obtener token
        let token = this.tokenC.getToken(req);
        //Decodificar token
        let decode = jwt.decode(token, process.env.NODE_PRIVATE_KEY);    
        //Obtener el id del usuario a partir del token
        let user_id = decode.id;    
        //Obtener productos por usuario
        Product.find({user_id}, (error, docs)=>{
            if(error){
                res.status(500).json({error});
            }else{
                res.status(200).json(docs);
            }
        });
    }

    get = (req, res)=>{
        Product.find((error, docs)=>{
            if(error){
                res.status(500).json({error});
            }else{
                res.status(200).json(docs);
            }
        });
    }

    update = (req, res)=>{
        //Obtener datos del producto para su posterior actualización
        let {id, name, price, url_img} = req.body;
        //Obtener token
        let token = this.tokenC.getToken(req);
        //Decodificar token
        let decode = jwt.decode(token, process.env.NODE_PRIVATE_KEY);    
        //Obtener el id del usuario a partir del token
        let user_id = decode.id;   
        //Actualizar producto por usuario en la base de datos
        /***
         * Primer parámetro ( {_id: id, user_id} ): objeto con las opciones de búsqueda en la BD
         * Segundo parámetro: ( {name, price} ): Campos/valores a actualizar en el documento
         * Tercer parámetro ( (error, doc)=>{} ): callback, función a ejecutarse cuando se envia la petición
         * *****/
        Product.findOneAndUpdate({_id: id, user_id}, {name, price, url_img}, (error, doc)=>{
            if(error){
                res.status(500).json(error);
            }else{
                res.status(200).json({info: 'Producto actualizado'});
            }
        });
    }

    delete = (req, res)=>{
        //Obtener id del producto del cuerpo de la petición
        let {id} = req.body;
        //Obtener token
        let token = this.tokenC.getToken(req);
        //Decodificar token
        let decode = jwt.decode(token, process.env.NODE_PRIVATE_KEY);    
        //Obtener el id del usuario a partir del token
        let user_id = decode.id;   
        Product.findOneAndRemove({_id: id, user_id}, (error, doc)=>{
            if(error){
                res.status(500).json({error});
            }else{
                if(doc){
                    res.status(200).json({removed: true});
                }else{
                    res.status(200).json({removed: false});
                }
                
            }
        });
    }

}

module.exports = ProductController;