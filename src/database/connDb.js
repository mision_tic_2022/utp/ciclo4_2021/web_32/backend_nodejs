const mongoose = require('mongoose');
const {db} = require('./urlDb');

class ConnDb{
    constructor(){
        this.connection();
    }

    async connection(){
        this.conn = await mongoose.connect(db);
        /*mongoose.connect(db).then(()=>{
            console.log("Conexión exitosa a la BD");
        });
        */
    }
}

module.exports = ConnDb;