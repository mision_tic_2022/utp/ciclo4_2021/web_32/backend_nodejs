const { Router } = require("express");
const ProductController = require("../controllers/productController");
const TokenController = require("../controllers/tokenController");

class ProductRouter {
  constructor() {
    this.router = Router();
    this.config();
  }

  config() {
    //Crear objeto ProductController
    const productC = new ProductController();    
    this.router.get('/product', productC.get);
    //Crear objeto TokenController
    const tokenC = new TokenController();
    //Middleware
    this.router.use(tokenC.verifyAuth);
    //Configurar/crear rutas
    this.router.post("/product", productC.create);    
    this.router.get('/product/user', productC.getByUser);
    this.router.put('/product', productC.update);
    this.router.delete('/product', productC.delete);
  }
}

module.exports = ProductRouter;
