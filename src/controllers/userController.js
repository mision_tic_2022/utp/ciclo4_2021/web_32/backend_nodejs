const User = require('../models/user');
const jsonwebtoken = require('jsonwebtoken');

class UserController {

    register(req, res) {
        let objUser = req.body;
        if (objUser.name && objUser.lastname && objUser.email && objUser.password) {
            User.create(objUser, (error, doc) => {
                if (error) {
                    res.status(500).json({ info: 'Error de inserción' });
                } else {
                    console.log(doc);
                    let token = jsonwebtoken.sign({id: doc._id}, process.env.NODE_PRIVATE_KEY);
                    res.status(201).json({ token });
                }
            });
        } else {
            res.status(400).json({ info: 'Datos incompletos' })
        }
    }

    login(req, res) {
        let { email, password } = req.body;
        User.find({ email, password }, (error, docs) => {
            if (error) {
                console.log(error);
                res.status(500).send();
            } else {
                //si el array es mayor a cero, las credenciales corresponden
                if (docs.length > 0) {
                    //Generar token
                    let token = jsonwebtoken.sign({id: docs[0]._id}, process.env.NODE_PRIVATE_KEY);
                    res.status(200).json({ token });
                }else{
                    res.status(401).json({info: 'Credenciales inválidas'});
                }
            }
        })
    }
}

module.exports = UserController;